```mermaid
graph LR
    subgraph Filesystem
        f1[(File 1)]
        f2[(File 2)]
        fo[(File O)]
    end
    s1([Server 1]) --> f1
    s1([Server 1]) --> f2
    sn([Server N]) --> fo
    f1 --> c1([Client 1])
    f2 --> c1([Client 1])
    fo --> c1([Client 1])
    f1 --> cm([Client M])
    f2 --> cm([Client M])
    fo --> cm([Client M])
```
