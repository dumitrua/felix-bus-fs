```mermaid
graph LR
    s1([Server 1]) --> f([Process])
    sn([Server N]) --> f
    f --> c1([Client 1])
    f --> cm([Client M])
```
