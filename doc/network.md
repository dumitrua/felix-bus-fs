```mermaid
graph LR
    s1([Server 1]) --> c1([Client 1])
    s1([Server 1]) --> cm([Client M])
    sn([Server N]) --> c1
    sn([Server N]) --> cm
```
