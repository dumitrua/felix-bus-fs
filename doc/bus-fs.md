```mermaid
graph LR
    s1([Server 1])
    s2([Server 2])
    sn([Server N])
    subgraph Filesystem
        f1[(Dir 1/File 1)]
        f2[(Dir 1/File 2)]
        f3[(Dir 2/File 1)]
        f4[(Dir 2/File 2)]
        f5[(Dir 2/File 3)]
        fo[(Dir 0/File 1)]
    end
    c1([Client 1])
    c2([Client 2])
    c3([Client 3])
    cm([Client M])
    s1 --> f1
    s1 --> f2
    s2 --> f3
    s2 --> f4
    s2 --> f5
    sn --> fo
    f1 --> c1
    f2 --> c1
    f1 --> c2
    f2 --> c2
    f3 --> c3
    f4 --> c3
    f5 --> c3
    fo --> cm
```
