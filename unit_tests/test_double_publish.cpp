#include <sys/types.h>
#include <unistd.h>

#include "catch.hpp"

#include "felixbus/felixbus.hpp"

using namespace felixbus;

TEST_CASE( "double publish data", "[error][server]" ) {

  char host[256];
  int rc = gethostname(host, 256);
  REQUIRE(rc == 0);
  std::string hostname(host);

  pid_t pid = getpid();
  REQUIRE(pid > 0);

  struct FelixBusInfo info;
  info.ip = "192.168.0.1";
  info.port = 12345;
  info.unbuffered = false;
  info.pubsub = true;
  info.raw_tcp = false;
  info.netio_pages = 16;
  info.netio_pagesize = 256;

  uint64_t fid = 0x1f9f346f00000900;
  std::string filename = "test";

  std::filesystem::path bus_path_prefix = "unit_tests";
  bus_path_prefix /= "data";
  bus_path_prefix /= "double_publish";

  FelixBus bus1(bus_path_prefix);
  // bus1.set_verbose(true);
  bus1.set_cleanup(false);

  std::error_code ec;
  bus1.publish(fid, filename, info, ec);
  REQUIRE(!ec);
  FelixBusInfo check_info = bus1.get_info(fid, ec);
  REQUIRE(!ec);
  REQUIRE(check_info.host == hostname);
  REQUIRE(check_info.pid == pid);

  // second time
  FelixBus bus2(bus_path_prefix);
  // bus2.set_verbose(true);

  bus2.publish(fid, filename, info, ec);
  REQUIRE(ec == std::errc::no_such_file_or_directory);

  // find out where and who
  check_info = bus2.get_info(fid, ec);
  REQUIRE(!ec);
  REQUIRE(check_info.host == hostname);
  REQUIRE(check_info.pid == pid);
  // REQUIRE(check_info.user == "");
}
