#include <sys/types.h>
#include <unistd.h>

#include "catch.hpp"

#include "felixbus/felixbus.hpp"

#include "felixbus/felixbus.h"

using namespace felixbus;

TEST_CASE( "publish data", "[error][server]" ) {

  char host[256];
  int rc = gethostname(host, 256);
  REQUIRE(rc == 0);
  std::string hostname(host);

  pid_t pid = getpid();
  REQUIRE(pid > 0);

  struct FelixBusInfo info;
  info.ip = "192.168.0.1";
  info.port = 12345;
  info.unbuffered = false;
  info.pubsub = true;
  info.raw_tcp = false;
  info.netio_pages = 16;
  info.netio_pagesize = 256;

  uint64_t fid = 0x1f9f346000090000;
  std::string filename = "test";

  std::filesystem::path bus_path_prefix = "unit_tests";
  bus_path_prefix /= "data";
  bus_path_prefix /= "publish";

  FelixBus bus;
  bus.set_cleanup(false);
  bus.set_verbose(false);
  bus.set_path(bus_path_prefix);
  bus.publish(fid, filename, info);

  std::filesystem::path bus_path = bus_path_prefix;
  bus_path /= "FELIX";
  bus_path /= "f9";
  bus_path /= "f346";
  bus_path /= filename + ".ndjson";

  if (!std::filesystem::exists(bus_path)) {
    bus_path = "felix-bus-fs" / bus_path;
    REQUIRE(std::filesystem::exists(bus_path));
  }

  REQUIRE(felix_bus_locked(bus_path.c_str()));

  std::error_code ec;

  FelixBusInfo check_info = bus.get_info(fid, ec);
  REQUIRE(!ec);
  REQUIRE(check_info.ip == info.ip);
  REQUIRE(check_info.port == info.port);
  REQUIRE(check_info.unbuffered == info.unbuffered);
  REQUIRE(check_info.raw_tcp == info.raw_tcp);

  REQUIRE(check_info.host == hostname);
  REQUIRE(check_info.pid == pid);
}
