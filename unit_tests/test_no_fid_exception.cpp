#include "catch.hpp"

#include "felixbus/felixbus.hpp"

using namespace felixbus;

TEST_CASE( "no fid lookup with exception", "[error][client]" ) {

  std::filesystem::path bus_path = "data";

  FelixBus bus;
  bus.set_path(bus_path);

  REQUIRE_THROWS_MATCHES(bus.get_info(0x1e0f346a00000001), std::system_error, Catch::Message("No such file or directory"));
}
