#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "docopt/docopt.h"

#include "felixbus/felixbus.hpp"

#include "felixtag.h"

using namespace felixbus;

static const char USAGE[] =
R"(felix-bus-publish - Publish (fake) info on fids on the bus.

    Usage:
      felix-bus-publish [options] <fids>...

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --verbose-bus                     Show bus information
      --bus-dir=<bus-directory>         Set bus directory [default: bus]
      --bus-groupname=<groupname>       Set groupname for bus to use [default: FELIX]
      --no-cleanup                      Leave bus files in place
      --sleep=<seconds>                 Sleep after sending [default: 30]
      --filename=<filename>             Filename to publish information to [default: test]
)";

int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string


  // Fake info
  struct FelixBusInfo info;
  info.ip = "192.168.0.1";
  info.port = 12345;
  info.unbuffered = false;
  info.pubsub = true;
  info.netio_pages = 16;
  info.netio_pagesize = 256;

  try {
    std::filesystem::path bus_path_prefix = args["--bus-dir"].asString();

    FelixBus bus(bus_path_prefix);
    bus.set_groupname(args["--bus-groupname"].asString());
    bus.set_verbose(args["--verbose-bus"].asBool());
    bus.set_cleanup(!args["--no-cleanup"].asBool());

    std::string filename = args["--filename"].asString();

    std::vector<std::string> fids = args["<fids>"].asStringList();
    for(std::vector<std::string>::iterator it = fids.begin(); it != fids.end(); ++it) {
      uint64_t fid = std::stoul(*it, 0, 0);
      bus.publish(fid, filename, info);
    }

    int sleep_time = args["--sleep"].asLong();
    if (sleep_time > 0) {
        printf("Sleeping %d s\n", sleep_time);
        std::this_thread::sleep_for(std::chrono::seconds(sleep_time));
    }
    bus.publish_close();
  } catch (std::invalid_argument const& error) {
    std::cerr << "Argument or option of wrong type" << std::endl;
    std::cout << std::endl;
    std::cout << USAGE << std::endl;
    exit(-1);
  }
}
