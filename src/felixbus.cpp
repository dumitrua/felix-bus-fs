#include <stdexcept>
#include <iostream>

#include "felixbus/felixbus.hpp"
#include "felixbus/felixbus.h"

#include "felix/felix_fid.h"

using namespace felixbus;

void FelixBus::publish(uint64_t fid, const std::string& filename, FelixBusInfo& info, std::error_code* ecptr) {
  if (ecptr) {
    ecptr->clear();
  }

  struct felix_bus_info bus_info;
  bus_info.ip = info.ip.c_str();
  bus_info.port = info.port;
  bus_info.unbuffered = info.unbuffered;
  bus_info.pubsub = info.pubsub;
  bus_info.raw_tcp = info.raw_tcp;
  bus_info.netio_pages = info.netio_pages;
  bus_info.netio_pagesize = info.netio_pagesize;

  felix_bus_set_verbose(verbose);
  felix_bus_set_cleanup(cleanup);

  felix_bus bus = nullptr;
  char* bus_path = felix_bus_path(bus_path_prefix.c_str(), groupname.c_str(), get_vid(fid), get_did(fid), get_cid(fid), filename.c_str());
  if (bus_path) {
    try {
      bus = bus_by_name.at(bus_path);
    } catch(const std::out_of_range&) {
      bus = felix_bus_open(bus_path);
      bus_by_name[bus_path] = bus;
    }
  }

  if (bus) {
    int rc = felix_bus_write(bus, fid, &bus_info);
    if (rc == 0) {
      free(bus_path);
      return;
    }
  }

  free(bus_path);
  auto error_code = std::make_error_code(std::errc::no_such_file_or_directory);
  if (ecptr) {
    *ecptr = error_code;
    return;
  }
  throw std::system_error(error_code);
}

void FelixBus::publish_close() {
  for (const auto & [name, bus] : bus_by_name) {
    if (verbose)
      printf("Closing %s\n", name.c_str());
    felix_bus_close(bus);
    felix_bus_release(bus);
  }
  bus_by_name.clear();
}

FelixBusInfo FelixBus::get_info(uint64_t fid, std::error_code* ecptr) {

  if (verbose)
    printf("Getting info for 0x%lx\n", fid);

  // uint8_t vid = (fid >> 60) & 0xF;
  uint8_t did = (fid >> 52) & 0xFF;
  uint32_t cid = (fid >> 36) & 0xFFFF;
  std::filesystem::path path = bus_path_prefix;
  path /= groupname;
  path /= int_to_hex(did, 1);
  path /= int_to_hex(cid, 1);

  std::string fid_hex = "0x" + int_to_hex(fid);
  FelixBusInfo info;
  felix_bus_set_verbose(verbose);

  if (verbose)
    printf("Looking for %s\n", path.c_str());

  if (std::filesystem::exists(path)) {
    for(auto& json_file: std::filesystem::directory_iterator(path)) {
      if (verbose)
        std::cout << "Looking for " << fid_hex << " in " << json_file.path() << std::endl;
      simdjson::dom::element fid_data;
      simdjson::dom::element data;

      bool stale = felix_bus_stale(json_file.path().c_str());
      if (verbose)
        std::cout << "Stale file " << json_file.path() << (ignore_stale ? " (ignored)" : " (NOT ignored)") << std::endl;
      if (ignore_stale && stale) {
        continue;
      }

      simdjson::dom::document_stream entries;
      auto error = parser.load_many(json_file.path()).get(entries);
      if (error) {
        if (verbose)
          std::cerr << "ERROR: " << error << std::endl;
        continue;
      }
      // we need the last entry in the file for this fid,
      // so we need to look through all of them...
      bool found = false;
      for (auto entry : entries) {
        // std::cout << "E" << entry << std::endl;

        auto no_data = entry.get(data);
        if (no_data) {
          if (verbose)
            std::cout << no_data << std::endl;
          continue;
        }

        uint64_t entry_fid;
        auto no_fid = data[BUS_FID].get(entry_fid);
        if (no_fid) {
          if (verbose)
            std::cout << no_fid << std::endl;
          continue;
        }

        if (entry_fid != fid) {
          continue;
        }

        if (verbose)
          std::cout << fid_hex << ": " << data << std::endl;

        // found one but it may not be the last one
        found = true;
        info.fid = data[BUS_FID];
        info.hfid = data[BUS_HFID];
        info.ip = data[BUS_IP];
        info.port = data[BUS_PORT];
        info.unbuffered = data[BUS_UNBUFFERED];
        info.pubsub = data[BUS_PUBSUB];
        info.netio_pages = data[BUS_NETIO_PAGES];
        info.netio_pagesize = data[BUS_NETIO_PAGESIZE];

        // compatibility for field(s) added in 4.3.x
        bool raw_tcp = false;
        auto no_raw_tcp = data[BUS_RAW_TCP].get(raw_tcp);
        info.raw_tcp = no_raw_tcp ? false : raw_tcp;

        // special fields
        info.host = data[BUS_HOST];
        uint64_t pid = 0;
        auto no_pid = data[BUS_PID].get(pid);
        if (!no_pid) {
          info.pid = (pid_t)pid;
        }
        info.user = data[BUS_USER];
      }

      if (found) {
        if (ecptr) {
          ecptr->clear();
        }
        return info;
      }
      // std::cout << entries.truncated_bytes() << " bytes "<< std::endl;
    }
  }
  // std::cout << "FID not found " << fid_hex << std::endl;
  auto error_code = std::make_error_code(std::errc::no_such_file_or_directory);
  if (ecptr) {
    *ecptr = error_code;
    return info;
  }
  throw std::system_error(error_code);
}
