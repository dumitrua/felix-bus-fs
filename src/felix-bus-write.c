#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "felixbus/felixbus.h"

int main(void) {
    char* bus_path;
    felix_bus bus;
    int rc;

    felix_bus_set_verbose(0);

    bus_path = felix_bus_path("bus", "FELIX", 0x1, 0xFF, 0x00001, "dma-0");
    if (bus_path == NULL) {
        printf("felix_bus_path: errno=%d str=%s\n", errno, strerror(errno));
        return 1;
    }

    bus = felix_bus_open(bus_path);
    if (bus == NULL) {
        printf("felix_bus_open: errno=%d str=%s\n", errno, strerror(errno));
        return 1;
    }

    uint64_t fid = 0x1ff0001000000200;
    struct felix_bus_info info;
    info.ip = "192.168.1.4";
    info.port = 56747;
    info.unbuffered = false;
    info.pubsub = true;
    info.netio_pages = 99;
    info.netio_pagesize = 64;

    rc = felix_bus_write(bus, fid, &info);
    if (rc < 0) {
        printf("felix_bus_write: errno=%d str=%s\n", errno, strerror(errno));
        return 1;
    }

    printf("B\n");

    sleep(10);

    info.unbuffered = true;

    printf("U\n");

    rc = felix_bus_write(bus, fid, &info);
    if (rc < 0) {
        printf("felix_bus_write: errno=%d str=%s\n", errno, strerror(errno));
        return 1;
    }

    rc = felix_bus_close(bus);
    if (rc < 0) {
        printf("felix_bus_close: errno=%d str=%s\n", errno, strerror(errno));
        return 1;
    }

    sleep(30);

    rc = felix_bus_release(bus);
    if (rc < 0) {
        printf("felix_bus_release: errno=%d str=%s\n", errno, strerror(errno));
        return 1;
    }
}
