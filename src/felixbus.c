#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <limits.h>
#include <pthread.h>
#include <pwd.h>
#include <signal.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "jWrite.h"
#include "felixbus/felixbus.h"

// // NOTE: possible different solution for locking files, not used by FELIX currently
// // from: https://stackoverflow.com/questions/29067893/flock-is-it-possible-to-merely-check-if-the-file-is-already-locked-without-a

// /* Open and exclusive-lock file, creating it (-rw-------)
//  * if necessary. If fdptr is not NULL, the descriptor is
//  * saved there. The descriptor is never one of the standard
//  * descriptors STDIN_FILENO, STDOUT_FILENO, or STDERR_FILENO.
//  * If successful, the function returns 0.
//  * Otherwise, the function returns nonzero errno:
//  *     EINVAL: Invalid lock file path
//  *     EMFILE: Too many open files
//  *     EALREADY: Already locked
//  * or one of the open(2)/creat(2) errors.
// */
// static int lockfile(const char *const filepath, int *const fdptr)
// {
//     struct flock lock;
//     int used = 0; /* Bits 0 to 2: stdin, stdout, stderr */
//     int fd;

//     /* In case the caller is interested in the descriptor,
//      * initialize it to -1 (invalid). */
//     if (fdptr)
//         *fdptr = -1;

//     /* Invalid path? */
//     if (filepath == NULL || *filepath == '\0')
//         return errno = EINVAL;

//     /* Open the file. */
//     do {
//         fd = open(filepath, O_RDWR | O_CREAT, 0600);
//     } while (fd == -1 && errno == EINTR);
//     if (fd == -1) {
//         if (errno == EALREADY)
//             errno = EIO;
//         return errno;
//     }

//     /* Move fd away from the standard descriptors. */
//     while (1)
//         if (fd == STDIN_FILENO) {
//             used |= 1;
//             fd = dup(fd);
//         } else
//         if (fd == STDOUT_FILENO) {
//             used |= 2;
//             fd = dup(fd);
//         } else
//         if (fd == STDERR_FILENO) {
//             used |= 4;
//             fd = dup(fd);
//         } else
//             break;

//     /* Close the standard descriptors we temporarily used. */
//     if (used & 1)
//         close(STDIN_FILENO);
//     if (used & 2)
//         close(STDOUT_FILENO);
//     if (used & 4)
//         close(STDERR_FILENO);

//     /* Did we run out of descriptors? */
//     if (fd == -1)
//         return errno = EMFILE;

//     /* Exclusive lock, cover the entire file (regardless of size). */
//     lock.l_type = F_WRLCK;
//     lock.l_whence = SEEK_SET;
//     lock.l_start = 0;
//     lock.l_len = 0;
//     if (fcntl(fd, F_SETLK, &lock) == -1) {
//         /* Lock failed. Close file and report locking failure. */
//         close(fd);
//         return errno = EALREADY;
//     }

//     /* Save descriptor, if the caller wants it. */
//     if (fdptr)
//         *fdptr = fd;

//     return 0;
// }

// void lock_example() {
//     int result;

//     result = lockfile("YOUR_LOCKFILE_PATH", NULL);
//     if (result == 0) {
//         /* Have an exclusive lock on YOUR_LOCKFILE_PATH */
//     } else
//     if (result == EALREADY) {
//         /* YOUR_LOCKFILE_PATH is already locked by another process */
//     } else {
//         /* Cannot lock YOUR_LOCKFILE_PATH, see strerror(result). */
//     }
// }

// // END NOTE

#define FELIX_BUS_TOUCH_INTERVAL 10 /* seconds */

struct felix_bus_s {
    int fd;
    char* path;
    timer_t timer;
    char host[256];
    pid_t pid;
    char* user;
};

static int felix_bus_verbose = 0;
static int felix_bus_cleanup = 1;
#define BUFFER_SIZE (500000)
static char buffer[BUFFER_SIZE];

int mkpath(char *dir, mode_t mode) {
    struct stat sb;

    if (!dir) {
        errno = EINVAL;
        return 1;
    }

    if (!stat(dir, &sb))
        return 0;

    mkpath(dirname(strdupa(dir)), mode);

    return mkdir(dir, mode);
}

void on_timer(union sigval val) {
    felix_bus bus = (felix_bus)val.sival_ptr;
    if (felix_bus_verbose)
        printf("on_timer\n");

    int rc = felix_bus_touch(bus);
    if (rc < 0) {
        printf("ERROR: Failed to touch bus file: %s\n", bus->path);
        printf("errno=%d str=%s\n", errno, strerror(errno));
    }
}

timer_t periodic_timer(int seconds, void* ptr) {
    pthread_attr_t attr;
    pthread_attr_init( &attr );

    struct sched_param parm;
    parm.sched_priority = 255;
    pthread_attr_setschedparam(&attr, &parm);

    struct sigevent sig;
    sig.sigev_notify = SIGEV_THREAD;
    sig.sigev_notify_function = on_timer;
    sig.sigev_value.sival_ptr = ptr;
    sig.sigev_notify_attributes = &attr;

    timer_t timer_id;
    int rc = timer_create(CLOCK_REALTIME, &sig, &timer_id);
    if (rc < 0) return NULL;

    struct itimerspec in;
    in.it_value.tv_sec = seconds;
    in.it_value.tv_nsec = 0;
    in.it_interval.tv_sec = seconds;
    in.it_interval.tv_nsec = 0;

    rc = timer_settime(timer_id, 0, &in, NULL);
    if (rc < 0) {
        timer_delete(timer_id);
        return NULL;
    }

    return timer_id;
}

// void felix_bus_on_exit(int exit_code, void *arg) {
// }

void felix_bus_set_verbose(int verbose) {
    felix_bus_verbose = verbose;
}

void felix_bus_set_cleanup(int cleanup) {
    felix_bus_cleanup = cleanup;
}

char* felix_bus_path(const char* bus_path_prefix, const char* groupname, uint8_t vid, uint8_t did, uint32_t cid, const char* bus_filename) {
    if (felix_bus_verbose)
        printf("felix_bus_path\n");

    char* bus_path;

    // to be freed by caller
    bus_path = (char*)malloc(PIPE_BUF*sizeof(char));

    // for future compatibility, DID and CID are encoded as shortest hexadecimal, VID is only used in decoding the FID
    int count = snprintf(bus_path, PIPE_BUF, "%s/%s/%x/%x/%s.ndjson", bus_path_prefix, groupname, did, cid, bus_filename);
    if ((count < 0) || (count >= PIPE_BUF)) {
        printf("ERROR: Cannot create bus_path\n");
        free(bus_path);
        return NULL;
    }

    char* bus_path_copy = strdup(bus_path);
    if (bus_path_copy == NULL) return NULL;

    char* dir = dirname(bus_path_copy);

    int rc = mkpath(dir, S_IWUSR | S_IRUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IXOTH);
    free(bus_path_copy);
    if (rc < 0) return NULL;

    if (felix_bus_verbose)
        printf("bus_path: %s\n", bus_path);

    return bus_path;
}

int felix_bus_locked(const char* bus_path) {
    /* Open file */
    /* NOTE: needs to be WR for it to be lockable */
    /* Relatively slow, use stale for faster status */
    int fd = open(bus_path, O_RDWR, 0);
    if (fd < 0) return 0;

    /* Try Lock file */
    int rc = flock(fd, LOCK_EX | LOCK_NB);
    if ((rc < 0) && (errno==EWOULDBLOCK)) {
        close(fd);
        return 1;
    }
    return 0;
}

int felix_bus_stale(const char* bus_path) {
    struct stat sb;

    int rc = stat(bus_path, &sb);
    if (rc < 0) {
        printf("ERROR: Failed to stat bus file: %s\n", bus_path);
        printf("errno=%d str=%s\n", errno, strerror(errno));
        return 1;
    }

    time_t now = time(0);
    if (now < 0) {
        printf("ERROR: Failed to stat bus file: %s\n", bus_path);
        printf("errno=%d str=%s\n", errno, strerror(errno));
        return 1;
    }

    double diff_time = difftime(now, sb.st_mtime);

    // if (felix_bus_verbose) {
    //     printf("Mod: %lu %s", sb.st_mtime, ctime(&sb.st_mtime));
    //     printf("Now: %lu %s", now, ctime(&now));
    //     printf("Dif: %f\n", diff_time);
    // }

    return diff_time > 2*FELIX_BUS_TOUCH_INTERVAL;
}

felix_bus felix_bus_open(const char* bus_path) {
    int rc;

    /* Open file */
    int fd = open(bus_path, O_CREAT | O_WRONLY | O_SYNC, S_IWUSR | S_IRUSR | S_IRGRP | S_IWGRP | S_IROTH );
    if (fd < 0) return NULL;

    /* Lock file */
    rc = flock(fd, LOCK_EX | LOCK_NB);
    if (rc < 0) return NULL;

    /* Truncate the file */
    rc = ftruncate(fd, 0);
    if (rc < 0) return NULL;

    /* create bus */
    felix_bus bus;
    bus = (felix_bus)malloc(sizeof(struct felix_bus_s));

    /* Process ID */
    bus->pid = getpid();

    /* Hostname */
    rc = gethostname(bus->host, 256);
    if (rc < 0) {
        free(bus);
        return NULL;
    }

    /* Username */
    long user_bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);
    if (user_bufsize == -1) {
        user_bufsize = 16384;
    }
    char *user_buf = malloc(user_bufsize);
    if (user_buf == NULL) {
        free(bus);
        return NULL;
    }
    uid_t uid = geteuid();
    struct passwd pwd;
    struct passwd *user;
    rc = getpwuid_r(uid, &pwd, user_buf, user_bufsize, &user);
    if ((rc < 0) || (user == NULL)) {
        free(bus);
        return NULL;
    }
    bus->user = strdup(pwd.pw_name);

    /* Free username memory */
    free(user_buf);

    /* Start a timer to keep the file current */
    timer_t timer = periodic_timer(FELIX_BUS_TOUCH_INTERVAL, bus);
    if (timer == NULL) {
        free(bus);
        return NULL;
    }

    /* Make sure we delete file and timer at the end */
    // rc = on_exit(felix_bus_on_exit, (void *)bus);
    // if (rc < 0) {
    //     free(timer);
    //     free(bus);
    //     return NULL;
    // }

    bus->fd = fd;
    bus->path = strdup(bus_path);
    bus->timer = timer;

    if (felix_bus_verbose)
        printf("felix_bus_open(%d) for %s\n", fd, bus_path);

    return bus;
}

int felix_bus_write(felix_bus bus, uint64_t fid, const struct felix_bus_info* info) {
    // FIXME check vid, cid and did fall within range

    char hex_fid[19];
    snprintf(hex_fid, 19, "0x%016lx", fid);

    struct jWriteControl jwc;
    jwOpen(&jwc, buffer, BUFFER_SIZE, JW_OBJECT, JW_NDJSON);

    jwObj_string(&jwc, BUS_HFID, hex_fid);
    jwObj_ulong(&jwc, BUS_FID, fid);
    jwObj_string(&jwc, BUS_IP, info->ip);
    jwObj_int(&jwc, BUS_PORT, info->port);
    jwObj_bool(&jwc, BUS_UNBUFFERED, info->unbuffered);
    jwObj_bool(&jwc, BUS_PUBSUB, info->pubsub);
    jwObj_bool(&jwc, BUS_RAW_TCP, info->raw_tcp);
    jwObj_int(&jwc, BUS_NETIO_PAGES, info->netio_pages);
    jwObj_int(&jwc, BUS_NETIO_PAGESIZE, info->netio_pagesize);

    jwObj_string(&jwc, BUS_HOST, bus->host);
    jwObj_int(&jwc, BUS_PID, bus->pid);
    jwObj_string(&jwc, BUS_USER, bus->user);

    int rc = jwClose(&jwc);
    if (rc != 0) {
        printf("ERROR: Failed to write to: %s\n", bus->path);
        printf("jwError=%d str=%s\n", rc, jwErrorToString(rc));
        return rc;
    }

    rc = write(bus->fd, buffer, strlen(buffer));
    if (rc < 0) {
        printf("ERROR: Failed to write to: %s\n", bus->path);
        printf("errno=%d str=%s\n", errno, strerror(errno));
    }

    return rc < 0 ? rc : 0;
}

int felix_bus_touch(felix_bus bus) {
    return bus->fd >= 0 ? futimens(bus->fd, NULL) : 0;
}

int felix_bus_close(felix_bus bus) {
    // Don't really close the file, otherwise we cannot keep the lock or touch the file
    return 0;
}

int felix_bus_release(felix_bus bus) {
    int rc;

    if (bus == NULL) return 0;

    // delete timer
    timer_t timer = bus->timer;
    if (felix_bus_verbose)
        printf("Deleting timer(%d) for: %s\n", bus->fd, bus->path);
    if (timer != NULL) {
        rc = timer_delete(timer);
        if (rc < 0) {
            printf("ERROR: Failed to delete timer for: %s\n", bus->path);
            printf("errno=%d str=%s\n", errno, strerror(errno));
        }
    }

    // close and remove file
    if (felix_bus_verbose)
        printf("Closing bus file(%d): %s\n", bus->fd, bus->path);
    rc = close(bus->fd);
    if (rc < 0) {
        printf("ERROR: Failed to remove bus file: %s\n", bus->path);
        printf("errno=%d str=%s\n", errno, strerror(errno));
    }
    bus->fd = -1;

    // cleanup files
    if (felix_bus_cleanup) {
        if (felix_bus_verbose)
            printf("Removing bus file: %s\n", bus->path);
        rc = unlink(bus->path);
        if (rc < 0) {
            printf("ERROR: Failed to remove bus file: %s\n", bus->path);
            printf("errno=%d str=%s\n", errno, strerror(errno));
        }

        char* dir = dirname(bus->path);
        if (felix_bus_verbose) printf("Trying to remove cid dir if empty: %s\n", dir);
        rmdir(dir);

        dir = dirname(dir);
        if (felix_bus_verbose) printf("Trying to remove did dir if empty: %s\n", dir);
        rmdir(dir);

        dir = dirname(dir);
        if (felix_bus_verbose) printf("Trying to remove version dir if empty: %s\n", dir);
        rmdir(dir);

        dir = dirname(dir);
        if (felix_bus_verbose) printf("Trying to remove group_name dir if empty: %s\n", dir);
        rmdir(dir);
    }

    free(bus->path);
    free(bus->user);
    free(bus);

    return 0;
}
