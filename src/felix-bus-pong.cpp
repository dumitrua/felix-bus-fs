#include <iostream>
#include <map>
#include <string>
#include <vector>

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include "docopt/docopt.h"

#include "felixbus/felixbus.hpp"

#include "felixtag.h"

using namespace felixbus;

extern "C" {
  void __gcov_dump(void);

  void my_handler(int signum) {
    printf("received signal\n");
#ifdef FELIX_COVERAGE
    __gcov_dump(); /* dump coverage data on receiving SIGINT */
#endif
  }
}

static const char USAGE[] =
R"(felix-bus-pong - Test to measure publish-lookup turnaround; to be used with felix-bus-ping.

    Usage:
      felix-bus-pong [options]

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --verbose-bus                     Show bus information
      --bus-dir=<bus-directory>         Set bus directory [default: bus]
      --bus-groupname=<groupname>       Set groupname for bus to use [default: FELIX_PING_PONG]
      --no-cleanup                      Leave bus files in place
)";

int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string


  // Add ctrl-c handler
  struct sigaction new_action, old_action;
  new_action.sa_handler = my_handler;
  sigemptyset(&new_action.sa_mask);
  new_action.sa_flags = 0;
  sigaction(SIGINT, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN) {
    sigaction (SIGINT, &new_action, NULL);
  }

  try {
    std::filesystem::path bus_path_prefix = args["--bus-dir"].asString();

    FelixBus bus(bus_path_prefix);
    bus.set_groupname(args["--bus-groupname"].asString());
    bus.set_verbose(args["--verbose-bus"].asBool());
    bus.set_cleanup(!args["--no-cleanup"].asBool());

    std::string filename_ping = "ping";
    uint64_t fid_ping = 0x1ff0007000000700;

    std::string filename_pong = "pong";
    uint64_t fid_pong = 0x1ff0007000000900;

    std::error_code prev_ec = std::make_error_code(std::errc());
    FelixBusInfo prev_info;
    while(true) {
      std::error_code ec;
      FelixBusInfo info = bus.get_info(fid_ping, ec);

      if (ec != prev_ec) {
        if (ec) {
          std::cout << ec.message() << " - " << ec << std::endl;
        }
        prev_ec = ec;
      }

      if (!ec) {
        if (info != prev_info) {
          std::cout << info << std::endl;

          bus.publish(fid_pong, filename_pong, info);

          prev_info = info;
        }
      }

      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    std::this_thread::sleep_for(std::chrono::seconds(30));
  } catch (std::invalid_argument const& error) {
    std::cerr << "Argument or option of wrong type" << std::endl;
    std::cout << std::endl;
    std::cout << USAGE << std::endl;
    exit(-1);
  }
}
